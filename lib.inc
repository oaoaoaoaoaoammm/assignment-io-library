section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, 60
    
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:              
     mov rax, -1
.loop:
    inc rax
    test byte [rdi+rax], 0xFF   
    jne .loop
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi             
    call string_length   
    pop rsi             
    mov rdx, rax           
    mov rax, 1          
    mov rdi, 1             
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1
    mov rdi, 1
    mov rsi, 0xA
    mov rdx, 1
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды
print_uint:

    xor rcx, rcx
    xor rax, rax
    dec rsp
    mov [rsp], al
    mov r10, 0xA
    mov rax, rdi
    
.loop:

    xor rdx, rdx
    div r10
    add dl, 0x30
    dec rsp
    mov [rsp], dl
    inc rcx
    test rax, rax
    jnz .loop
    
    mov rdi, rsp
    push rcx
    
    call print_string
    
    pop rcx
    add rsp, rcx
    inc rsp
    
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:

    
    cmp rdi, 0x0
    jge .arn ; значит число положительное
    
    mov r10, rdi
    mov rdi, 0x2D ; знак "-"
    push r10
    
    call print_char
    
    mov rdi, r10
    neg rdi
    pop r10
    
.arn:
    call print_uint ;выводим число
    
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:

    push r10
    push rdi
    push rsi
    
    call string_length
    mov r10, rax   ;polychaem dliny 1 stroki
    mov rdi, rsi
    
    call string_length
    mov r8, rax   ;polychaem dliny 2 stroki
    
    pop rsi
    pop rdi
    
    cmp r10, r8 ;cnachala sravnivaem na dliny
    je .el
    
.nel:
    pop r10
    mov rax, 0x0
    ret
       
.el:
    cmp r10, 0x0
    je .ex
    cmpsb 		;сравнение последовательности байтов из участка памяти
    jne .nel 	;esli ne ravni stroki
    dec r10
    loop .el
    
    
.ex:
    pop r10
    mov rax, 1
    
    ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

    push 0x0
    mov rax, 0x0   
    mov rdi, 0x0   
    mov rsi, rsp 
    mov rdx, 1
    
    syscall
    
    pop rax
    
    ret

    
    
    
    

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:

	xor rcx, rcx
	xor r10, r10
	xor r8, r8
	
.loop 
	
	push rcx
	push rsi
	push rdi
	call read_char
	pop rdi
	pop rsi
	pop rcx
	
	cmp rcx, rsi ; esli byfer menshe slova
	jge .err
	
	cmp rax, 0x0 ; elsi konec
	je .finish
	
	cmp rcx, 0x0
	jne .check
	
	cmp rax, 0x20
	je .loop
	
	cmp rax, 0x9
	je .loop
	
	cmp rax, 0x10
	je .loop
	
.check
	cmp rax, 0x20
    je .finish

    cmp rax, 0x9
    je .finish

    cmp rax, 0x10
    je .finish
    
    mov [rdi+rcx], rax
    inc rcx
    jmp .loop	
	
.finish
	mov byte[rdi+rcx], 0x0
	mov rdx, rcx
	mov rax, rdi
	ret
	
.err 
	mov rax, 0x0
	ret
	


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:



    mov r10, 0xA
    mov r11, 0x0
    mov rcx, 0x0
    mov rax, 0x0

   
    
    
    
    .num:               
        cmp byte[rdi+rcx], 47    
        jb .not_symb             
        cmp byte[rdi+rcx], 58    
        ja .not_symb
        
    ;Если не проходит проверку то мы переходим к 
    ;not.symb иначе мы идём в .enter                                               
    
    
    .enter:
        mul r10                     
        mov r11b, byte[rdi+rcx]      
        sub r11b, 0x30              
        add rax, r11                
        inc rcx
        jmp .num
        ; переходим к .num 
    
    
    .not_symb:
        mov rdx, rcx
                 
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
	xor r10, r10
	mov rdx, 0x0


    cmp byte [rdi], 0x2D
    jne .arn ; elsi bezznak
    
    
    inc rdi
    
    
    call parse_uint
    
    cmp rdx, 0x0
    je .ex
    
    neg rax
    inc rdx
    jmp .ex
    
.arn:
    call parse_uint
    
.ex:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    xor rax, rax
    xor r10, 0x0
    
.arn:
    cmp rax, rdx
    je .narn
    
    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rsi + rax], 0x0
    je .ex
    
    inc rax
    jmp .arn
    
.narn:
    mov rax, 0x0
    
.ex:
    ret

